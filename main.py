import discord
from discord.ext import commands
import asyncio, aiomysql
import time, sys, traceback

import config

initial_extensions = (
    'cogs.settings',
    'cogs.events',
    'cogs.moderator',
    'cogs.statistics',
    'cogs.admin',
)

def get_prefix():
    """ Функция, возвращаемая возможные префиксы бота """

    return (
        'e/', 'e/ ', 'e.', 'e. ', # eng
        'е/', 'е/ ', 'е.', 'е. ', # ru
        'э/', 'э/ ', 'э.', 'э. ', # ru

        'el/', 'el/ ', 'el.', 'el. ', # eng
        'ел/', 'ел/ ', 'ел.', 'ел. ', # ru
        'эл/', 'эл/ ', 'эл.', 'эл. ', # ru

        'elz/', 'elz/ ', 'elz.', 'elz. ', # eng
        'елз/', 'елз/ ', 'елз.', 'елз. ', # ru
        'элз/', 'элз/ ', 'элз.', 'элз. ', # ru

        'Elizabeth/', 'Elizabeth/ ', 'Elizabeth.', 'Elizabeth. ', # eng
        'Элизабет/', 'Элизабет/ ', 'Элизабет.', 'Элизабет. '  # ru
    )

# Настраиваем бота
bot = commands.Bot(command_prefix=commands.when_mentioned_or(*get_prefix()), description=config.discord['description'])

# Подгружаем все модули
if __name__ == '__main__':

    # Загружаем основные модули в папке cogs
    for extension in initial_extensions:
        try:
            bot.load_extension(extension)
        except Exception as eroor:
            print(f'Невозможно загрузить расширение {extension}.', file=sys.stderr)
            traceback.print_exc()

@bot.event
async def on_ready():
    """ Событие при запуске бота """

    bot.start_at = int(time.time())

async def create_pool(loop):
    """ Функция для создания пула соединений с MySQL """

    bot.pool = await aiomysql.create_pool(**config.database,
        cursorclass=aiomysql.DictCursor,
        loop=loop
    )

# Запускаем пул
bot.loop.run_until_complete(create_pool(bot.loop))

# Запускаем бота
bot.run(config.discord['token'], bot=True, reconnect=True)
