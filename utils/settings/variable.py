from .set import Set

class Variable(Set):
    feature = None
    avaible_values = None

    def __init__(self, *, data):
        self.id = data['id']
        self.feature = data['feature']
        self.guild = data['guild']
        self.name = data['name']
        self.value = data['value']
        self.avaible_values = data['avaible_values']
        self.short_description = data['short_description']
        self.description = data['description']
        self.enable = data['enable']
        self.connection_pool = data['connection_pool']

    async def update(self, new_value):
        await self._update('variables', new_value)
