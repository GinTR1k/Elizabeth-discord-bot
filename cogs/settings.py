import discord
from discord.ext import commands

import json
import os.path
import asyncio

from ext.settings import *

class Settings:
    def __init__(self, bot):
        self.bot = bot

        # Текущая версия модуля
        self.__version__ = '1.0.0'

        # Установка цвета полоски в сообщениях Embed для всего модуля
        self.embed_color = discord.Colour(0xffff00)

    @commands.command(name='настройки', aliases=['settings'], help='Команда для отображения возможностей и настроек')
    @commands.guild_only()
    @commands.has_permissions(administrator=True)
    async def settings(self, ctx):
        """ Показать помощь """

        # Создаем Embed
        embed = discord.Embed(title="Elizabeth - Настройки", colour=self.embed_color,
            description="Настройки состоят из двух типов: __возможности__ и __переменные__ (или же настройки).\n" +\
                        "**Возможность** — это отдельные большие модули, которые позволяют, скажем, вести статистику. " +\
                        "Их можно отключать и включать. Главное отличие от переменных в том, что возможности имеют свои настройки, "+\
                        "которые используются исключительно в рамках той или иной возможности.\n" +\
                        "**Переменные** — это маленькие настройки, такие как канал по-умолчанию, куда бот будет " +\
                        "отсылать сообщения информационного характера, или же слова-приветствия, на которые реагирует бот " +\
                        "и ставит реакцию на сообщение с одним из данных слов. Переменные есть двух типов: глобальные и " +\
                        "относящиеся к какой-либо возможности. **Глобальные переменные** используются для настройки бота в целом, " +\
                        "например, установка все того же канала по-умолчанию, куда бот будет отправлять сообщения об " +\
                        "ошибках или информацию какую-либо. **Переменные, относящиеся к возможности**, используются " +\
                        "исключительно той возможностью, которому принадлежит данная переменная. " +\
                        "**Пример:** переменная `pubg_news_channel` принадлежит возможности `PUBG_News` — она [переменная] " +\
                        "позволяет указать канал, куда будут отправляться свежие новости. Если канал не будет указан, то " +\
                        "новости будут отправляться в канал по-умолчанию, за который отвечает глобальная переменная " +\
                        "`default_channel`. Если уже у глобальной переменной не будет указан канал, то бот будет отправлять " +\
                        "сообщения в первый текстовый канал, который найдет на сервере."
        )
        embed.set_footer(text=f'{self.__class__.__name__}, v{self.__version__}')

        embed.add_field(name="Список возможностей", value=f"`{ctx.prefix}{f'` или `{ctx.prefix}'.join([self.features.name, *self.features.aliases])}`", inline=False)
        embed.add_field(name="Информация о возможности", value=f"`{ctx.prefix}{f' <имя_возможности>` или `{ctx.prefix}'.join([self.features.name, *self.features.aliases])} <имя_возможности>`", inline=False)
        embed.add_field(name="Cписок глобальных переменных", value=f"`{ctx.prefix}{f'` или `{ctx.prefix}'.join([self.variables.name, *self.variables.aliases])}`", inline=False)
        embed.add_field(name="Информация о переменной", value=f"`{ctx.prefix}{f' <имя_переменной>` или `{ctx.prefix}'.join([self.variables.name, *self.variables.aliases])} <имя_переменной>`", inline=False)

        await ctx.send(embed=embed)
        return

    @commands.group(name='возможности', aliases=['features'], help='Показать возможности')
    @commands.guild_only()
    @commands.has_permissions(administrator=True)
    async def features(self, ctx, feature=None, new_value=None):

        if new_value is not None:

            await ctx.invoke(self.feature_switch, feature, new_value)
            return

        # Если указана возможность
        if feature is not None:

            # Вызываем команду about, относящейся к возможностям
            await ctx.invoke(self.feature_about, feature)
            return

        # Если не было вызвано подкоманды
        if ctx.invoked_subcommand is None:

            guild = await create_guild(ctx.guild.id, self.bot.pool)

            # Создаем Embed
            embed = discord.Embed(title="Elizabeth - Настройки: Возможности", colour=self.embed_color,
                description=f"Используйте команду следующим образом для просмотра более подробной информации о возможностях: " +\
                            f"`{ctx.prefix}{ctx.invoked_with} <имя_возможности>`."
            )
            embed.set_footer(text=f'{self.__class__.__name__}, v{self.__version__}')

            # Выводим возможности и их описание в Embed
            for feature_id, feature in guild.features.items():
                if feature.enable == 0:
                    continue

                embed.add_field(name=f"{feature.short_description}", value=f"`{ctx.prefix}{ctx.invoked_with} {feature.name}`", inline=False)

            await ctx.send(embed=embed)
            return

    @features.command(name='about', help='Подробная информация о возможности')
    async def feature_about(self, ctx, feature_name):

        guild = await create_guild(ctx.guild.id, self.bot.pool)

        # Получаем запись о данной возможности по имени
        for feature_id, feature in guild.features.items():
            if feature.name != feature_name:
                continue

            # Создаем Embed
            embed = discord.Embed(title=f"Elizabeth - Настройки: Возможность {feature.name}", colour=self.embed_color, description=f"{feature.description}")
            embed.set_footer(text=f'{self.__class__.__name__}, v{self.__version__}')
            if feature.id != 1:
                print(feature.value)
                embed.add_field(name=f"Включить/Выключить возможность", value=f"`{ctx.prefix}{ctx.invoked_with} {feature.name} <включить/выключить>`", inline=False)
                embed.add_field(name=f"Текущее Состояние", value="__Включено__" if feature.value == 1 else "__Выключено__", inline=False)

            # Получаем список всех переменных по данной возможности
            for variable in feature.variables:
                embed.add_field(name=f"{variable.short_description}", value=f"`{ctx.prefix}переменные {variable.name}`", inline=False)

            await ctx.send(embed=embed)
            return

        # Создаем Embed
        embed = discord.Embed(title=f"Elizabeth - Настройки: Возможности {feature_name}", colour=self.embed_color, description="Данная возможность не найдена.")
        embed.set_footer(text=f'{self.__class__.__name__}, v{self.__version__}')

        await ctx.send(embed=embed)
        return

    @features.command(name='switch', help='Включить/выключить возможность на данном сервере')
    async def feature_switch(self, ctx, feature_name, new_value):
        guild = await create_guild(ctx.guild.id, self.bot.pool)

        for feature_id, feature in guild.features.items():
            if feature.name != feature_name:
                continue

            if feature.enable == 0 and ctx.author.id != 267238303311331328:
                continue

            embed = None

            if new_value.lower() not in ['true', 'false', 'включить', 'выключить', 'отключить']:
                embed = discord.Embed(title=f"Elizabeth - Настройки: Возможность {feature.name}",
                    colour=self.embed_color,
                    description=f"Параметр <новое_значение> имеет недопустимое значение."
                )
                embed.set_footer(text=f'{self.__class__.__name__}, v{self.__version__}')

                await ctx.send(embed=embed)
                return

            if new_value.lower() in ['false', 'выключить', 'отключить']:
                await feature.update(False)

                embed = discord.Embed(title=f"Elizabeth - Настройки: Возможность {feature.name}",
                    colour=self.embed_color,
                    description=f"Данная возможность выключена."
                )
                embed.set_footer(text=f'{self.__class__.__name__}, v{self.__version__}')

                await ctx.send(embed=embed)
                return

            if new_value.lower() in ['true', 'включить']:
                await feature.update(True)

                embed = discord.Embed(title=f"Elizabeth - Настройки: Возможность {feature.name}",
                    colour=self.embed_color,
                    description=f"Данная возможность включена."
                )
                embed.set_footer(text=f'{self.__class__.__name__}, v{self.__version__}')

                await ctx.send(embed=embed)
                return

        embed = discord.Embed(title=f"Elizabeth - Настройки: Возможности {feature_name}", colour=self.embed_color, description="Данная возможность не найдена.")
        embed.set_footer(text=f'{self.__class__.__name__}, v{self.__version__}')

        await ctx.send(embed=embed)
        return

    @commands.group(name='переменные', aliases=['variables'], help='Показать переменные')
    @commands.guild_only()
    @commands.has_permissions(administrator=True)
    async def variables(self, ctx, variable_name=None):

        # Если указана переменная
        if variable_name is not None:

            # Вызываем команду about, относящейся к переменной
            await ctx.invoke(self.variable_about, variable_name)
            return

        # Если не было вызвано подкоманды
        if ctx.invoked_subcommand is None:

            # Создаем Embed
            embed = discord.Embed(title="Elizabeth - Настройки: Глобальные переменные", colour=self.embed_color,
                description=f"Используйте команду следующим образом для просмотра более подробной информации о переменных: " +\
                            f"`{ctx.prefix}{ctx.invoked_with} <имя_переменной>`.\n" +\
                            f"Полную помощь вы можете посмотреть, написав `{ctx.prefix}настройки`."
            )
            embed.set_footer(text=f'{self.__class__.__name__}, v{self.__version__}')

            await ctx.send(embed=embed)
            return

    @variables.command(name='about', help='Подробная информация о переменной')
    async def variable_about(self, ctx, variable_name):

        guild = await create_guild(ctx.guild.id, self.bot.pool)

        for feature_id, feature in guild.features.items():
            for variable in feature.variables:

                if variable.name != variable_name:
                    continue

                if variable.enable == 0 and ctx.author.id != 267238303311331328:
                    continue

                # Создаем Embed
                embed = discord.Embed(title=f"Elizabeth - Настройки: Переменная {variable.name}",
                    colour=self.embed_color,
                    description=f"{variable.description}\n**Данная переменная относится к возможности __{variable.feature.name}__**."
                )
                embed.set_footer(text=f'{self.__class__.__name__}, v{self.__version__}')

                # Разделяем доступные значения
                avaible_values = variable.avaible_values.split(':')

                # Инициализируем переменную
                avaible_values_text = ''
                value_text = ''

                for a_value in avaible_values:

                    # Если переменная - bool, преобразуем
                    if a_value == 'bool':
                        avaible_values_text += '`true` — включить, `false` — выключить\n'

                        if variable.value == 1:
                            value_text = '`true` (включено)'

                        elif variable.value == 0:
                            value_text = '`false` (выключено)'
                        continue

                    # Если переменная - канал, получаем канал
                    if a_value == 'text_channel':
                        avaible_values_text += '#канал — текстовый канал\n'

                        if isinstance(variable.value, int):
                            guild_channel = self.bot.get_channel(variable.value)

                            if guild_channel is not None:
                                value_text = guild_channel.mention
                            else:
                                value_text = f'Канал с ID {variable.value} не найден. Возможно, вы удалили данный канал.'
                        continue

                if variable.value is None:
                    value_text = 'Значение отсутствует'

                embed.add_field(name="Текущее значение", value=f"{value_text}", inline=False)
                embed.add_field(name="Изменить", value=f"`{ctx.prefix}{ctx.invoked_with} [Новое значение]`", inline=False)
                embed.add_field(name="Может принимать значения", value=f"{avaible_values_text}", inline=False)

                if variable.enable == 0:
                    embed.add_field(name="Примечание", value=f"Данная переменная отключена", inline=False)

                await ctx.send(embed=embed)
                return

        # Создаем Embed
        embed = discord.Embed(title=f"Elizabeth - Настройки: Переменные {feature_name}", colour=self.embed_color, description="Данная переменная не найдена.")
        embed.set_footer(text=f'{self.__class__.__name__}, v{self.__version__}')

        await ctx.send(embed=embed)
        return

def setup(bot):
    bot.add_cog(Settings(bot))
