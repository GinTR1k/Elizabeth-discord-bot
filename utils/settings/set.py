import abc

class Set(object):
    __metaclass__ = abc.ABCMeta

    guild = None
    id = None
    name = None
    value = None
    short_description = None
    description = None
    enable = None
    connection_pool = None

    async def _update(self, row_name, new_value):
        async with self.connection_pool.acquire() as connection:
            async with connection.cursor() as cursor:

                sql = f"UPDATE `guilds` SET `{row_name}` = JSON_SET(`{row_name}`, '$.%s', %s) WHERE `id` = %s"
                sql_parameters = (self.id, new_value, self.guild.id)

                await cursor.execute(sql, sql_parameters)

    @abc.abstractmethod
    async def update(self, new_value):
        """ Функция для вызова внутренней функции self._update """
        return
